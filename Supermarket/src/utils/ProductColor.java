/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Cao
 */
public class ProductColor {
    private Color[] colours = null;
    
    public ProductColor() {
        colours = readFile("color.txt");
    }
    
    public Color[] getColours() {
        return colours;
    }
    
    public Color[] readFile(String filename){
        Color[] colors = new Color[45];
        try {
            FileInputStream fstream = new FileInputStream(filename);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            int count = 0;
            while ((strLine = br.readLine()) != null){
                String[] info = strLine.split("\\s");
                colors[count] = new Color(Integer.parseInt(info[0]), Integer.parseInt(info[1]), Integer.parseInt(info[2]));
                count++;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductColor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductColor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return colors;
    }
}
