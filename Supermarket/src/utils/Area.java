/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;

/**
 *
 * @author Cao
 */
public class Area{
    private int x = 0;
    private int y = 0;
    private int width = 0;
    private int height = 0;
    private Point anglePoint = null;
    private Point[] points = null;// mang chua toa do 4 diem p1,p2,p3,p4
    private Polygon polygon = null;
    
    public Area(int x, int y, int width, int height, int angle) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        anglePoint = new Point(x, y);
        points = getOriginalPoints();
        rotatePointMatrix(getOriginalPoints(),angle,points);
        polygon = polygonize(points);
    }
    
    public Point[] getOriginalPoints(){

        /* In this example the rotated "polygon" are stored in this method. 
         * The Point is that if we want to rotate a polygon constatnly/frequently
         * we cannot use the values of an already rotated polygon as this will 
         * lead to the polygon deforming severely after few translations due 
         * to the points being constantly rounded. So the trick is to save the
         * original Points of the polygon and always rotate that one to the new
         *  angle instead of rotating the same one again and again.
        */
        Point[] originalPoints = new Point[4];
        
        originalPoints[0]= new Point(x, y);
        int newX = x + width;
        originalPoints[1]= new Point(newX, y);
        int newY = y + height;
        originalPoints[2]= new Point(newX, newY);
        originalPoints[3]= new Point(x, newY);
        
        return originalPoints;
    }
    
    public Polygon polygonize(Point[] polyPoints){
        //a simple method that makes a new polygon out of the rotated points
        Polygon tempPoly = new Polygon();
         for(int  i=0; i < polyPoints.length; i++){
             tempPoly.addPoint(polyPoints[i].x, polyPoints[i].y);
        }
        return tempPoly;
    }
    
    public void rotatePointMatrix(Point[] origPoints, double angle, Point[] storeTo){
        /* We ge the original points of the polygon we wish to rotate
         *  and rotate them with affine transform to the given angle. 
         *  After the opeariont is complete the points are stored to the 
         *  array given to the method.
        */
        AffineTransform.getRotateInstance(Math.toRadians(angle), anglePoint.x, anglePoint.y).transform(origPoints,0,storeTo,0,4);

    }

    public Polygon getPolygon() {
        return polygon;
    }
}
