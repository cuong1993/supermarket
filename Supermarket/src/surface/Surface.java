/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package surface;

import utils.Area;
import models.Drawer;
import utils.ProductColor;
import listener.AreaListener;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;
import javax.swing.JPanel;
import models.Product;
import models.SupermarketArea;
import network.Client;

/**
 *
 * @author Cao
 */
public class Surface extends JPanel{
    private ProductColor productColor = null;
    private Color[] colors = null;
    public ArrayList<SupermarketArea> areas = null;
    private Area[] rects = null;
    public static int pos = -1;
    public static int posDrawer = -1;
    private boolean isCreateArea = true;
    public static boolean isDrawMouseArea = false;
    public static int xChoosed = 0;
    public static int yChoosed = 0;
    public static int wChoosed = 0;
    public static int hChoosed = 0;
    private boolean init = true;
    private int areanumber = 0;
    private final int widthCell = 20;
    private final int heightCell = 10;
    private final AreaListener zoomListener;
    public static Client client = null;
    
    public Surface(ArrayList<SupermarketArea> lstArea,Client client){
        areas = lstArea;
        this.client = client;
        this.client.sf = this;
        this.zoomListener = new AreaListener(this);
        this.addMouseListener(zoomListener);
        this.addMouseMotionListener(zoomListener);
        this.addMouseWheelListener(zoomListener);
        initUI();
    }

    public Surface(int minZoomLevel, int maxZoomLevel, double zoomMultiplicationFactor) {
        this.zoomListener = new AreaListener(this, minZoomLevel, maxZoomLevel, zoomMultiplicationFactor);
        this.addMouseListener(zoomListener);
        this.addMouseMotionListener(zoomListener);
        this.addMouseWheelListener(zoomListener);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(10000, 10000);// set size 
    }
    public void setValue()
    {
        areanumber = areas.size();// so luong khu vuc
        this.zoomListener.setAreas(areas);
    }
    private void initUI(){
        
        productColor = new ProductColor();
        colors = productColor.getColours();
        areanumber = areas.size();// so luong khu vuc
        rects = new Area[areanumber]; // mang khu vuc chua cac mang diem, chua x,y,width,height,angle
        this.zoomListener.createPolygons(areanumber);
        this.zoomListener.setAreas(areas);
    }
    
    private void doDrawing(Graphics g){     
        for(int areaCode = 0; areaCode < areanumber; areaCode++){
            SupermarketArea area = areas.get(areaCode);// lay thong tin moi Suppermarket
            int posX = area.getPosX();
            int posXDefault = posX;
            int posY = area.getPosY();
            int posYDefault = posY;
            int angle = area.getAngle();

            
            int shelfheight = area.getShelfheight();// hieght shelf
            int quantity = area.getShelfquantity();// number shelfs
            int drawernumber = area.getDrawernumber(); // number drawer shelf
            int drawertotal = quantity * drawernumber; // Tong so o trong mot day
            
            Graphics2D g2d = (Graphics2D) g.create();
            Drawer rect = new Drawer(posXDefault, posYDefault, widthCell * drawertotal, heightCell * shelfheight);
            if(isCreateArea){
                g2d.setColor(Color.gray);
                Area r = new Area(posXDefault, posYDefault, widthCell * drawertotal, heightCell * shelfheight, angle);
                rects[areaCode] = r;
                this.zoomListener.polygons[areaCode] = rects[areaCode].getPolygon();
            }
            
            configureGraphics2D(g2d);
            g2d.rotate(Math.toRadians(angle), posX, posY);
            drawDrawers(g2d, area, shelfheight, drawertotal, posX, posY);
            
            if(pos == areaCode){
                posDrawer = pos;
                g2d.setColor(Color.red);
                BasicStroke bs = new BasicStroke(2, BasicStroke.CAP_ROUND,BasicStroke.JOIN_MITER);
                g2d.setStroke(bs);
                g2d.draw(rect); 
            }else{
                g2d.draw(rect);
            }
            g2d.dispose();
        }
        pos = -1;
        isCreateArea = false;
    }
    
    private void configureGraphics2D(Graphics2D g2d){
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        rh.put(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHints(rh);

        BasicStroke bs2 = new BasicStroke(1, BasicStroke.CAP_ROUND,BasicStroke.JOIN_MITER);
        g2d.setStroke(bs2);
        if (init) {
            // Initialize the viewport by moving the origin to the center of the window,
            // and inverting the y-axis to point upwards.
            init = false;
            Dimension d = getSize();
            g2d.scale(1, 1);
            // Save the viewport to be updated by the ZoomAndPanListener
            zoomListener.setCoordTransform(g2d.getTransform());
        } else {
            // Restore the viewport after it was updated by the ZoomAndPanListener
            g2d.setTransform(zoomListener.getCoordTransform());
        }
    }
    private Drawer isSimilarDrawer(ArrayList<Drawer> lstZRect,int valueX, int valueY)
    {
        for(int pos = 0; pos < lstZRect.size(); pos++)
        {
            Drawer d = lstZRect.get(pos);
            if(valueX == d.x  && valueY == d.y )
                return d;
        }
        return null;
    }
    private void drawDrawers(Graphics2D g2d, SupermarketArea area, int shelfheight, int drawertotal,int posX, int posY){
        ArrayList<Product> lstProducts = area.getListProducts();
        int productquantity = lstProducts.size();// get number product
        int posXDefault = posX;
        ArrayList<Drawer> lstZRect = new ArrayList<Drawer>();
        for(int id = 0; id < productquantity; id++)
        {
            Product product = lstProducts.get(id);
            int col =  product.getPosCol() - 1;
            int row =  product.getPosRow() - 1;
            int x = (col * widthCell) + posX;
            int y = (row * heightCell) + posY;
            Drawer zrect = new Drawer(x, y, widthCell * product.getContainer(), heightCell);
            g2d.setColor(colors[product.getColorCode()]);
            g2d.fill(zrect);
            if(product.getContainer() == 2)
            {
                zrect.vtIJ_Drawer = 2;
//                Drawer tmp = new Drawer(x + widthCell, y, widthCell, heightCell);
//                g2d.fill(tmp);
            }
            lstZRect.add(zrect);
        }
        for(int i = 1; i <= shelfheight; i++){
            for(int j = 1; j <= drawertotal; j++){
                Drawer tmp = isSimilarDrawer(lstZRect, posX, posY);
                if(tmp != null)
                {
                    g2d.setColor(Color.black);
                    g2d.draw(tmp);
                    posX += tmp.width;
                    if(tmp.vtIJ_Drawer == 2)
                    {
                        j++;
                    }
                    continue;
                }
                Drawer zrect = new Drawer(posX, posY, widthCell, heightCell);
                g2d.setColor(Color.black);
                g2d.draw(zrect);
                posX += widthCell;
            }
            posX = posXDefault;
            posY += heightCell;
        }
    }
    
    private void drawMouseArea(Graphics g){
        Graphics2D g2d = (Graphics2D) g.create();
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        rh.put(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHints(rh);

        BasicStroke bs2 = new BasicStroke(2, BasicStroke.CAP_ROUND,BasicStroke.JOIN_MITER);
        g2d.setStroke(bs2);
        if(isDrawMouseArea){
            g2d.setColor(Color.blue);
            drawPerfectRect(g2d, xChoosed, yChoosed, wChoosed, hChoosed);
        }
        g2d.dispose();
    }
    
    public void drawPerfectRect(Graphics g, int x, int y, int x2, int y2) {
            int px = Math.min(x,x2);
            int py = Math.min(y,y2);
            int pw = Math.abs(x-x2);
            int ph = Math.abs(y-y2);
            g.drawRect(px, py, pw, ph);
    }
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        doDrawing(g);
        drawMouseArea(g);
    }
}
