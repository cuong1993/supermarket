/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package surface;

import models.Drawer;
import utils.ProductColor;
import models.PointDrawer;
import listener.DetailListener;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import javax.swing.JPanel;
import models.Product;
import models.SupermarketArea;
import network.Client;

/**
 *
 * @author Cao
 */
public class DetailSurface extends JPanel{
    private final DetailListener detailListener;
    private boolean init = true;
    private SupermarketArea area = null;
    private final int posXDefault = 50;
    private final int posYDefault = 50;
    private final int width = 30;
    private final int height = 20;
    private int numberDrawer = 0;
    private int numbershelf = 0;
    private int totalDrawer = 0;
    private int numberHeight = 0;
    private ProductColor productColor = null;
    private Color[] colours = null;
    private ArrayList<Product> lstProduct = null;
    public static Drawer [][]m_Drawers = null;
    public static ArrayList<Drawer> shelf = null;
    public static PointDrawer pDrawer = null;
    public static Drawer objectMove = null;
    private Client client = null;
    public static Product productInfo = null;
    public static Point pMouseMove = null;
    public static boolean isMouseMove = false;
    public static Drawer rButton = null;
    public static ArrayList<String> idProduct = null;
    public void setArea(SupermarketArea area) {
        this.area = area;
    }

    public void setLstProduct(ArrayList<Product> lstProduct) {
        this.lstProduct = lstProduct;
    }
    
    public DetailSurface(SupermarketArea area, Client client){
        setLayout(new BorderLayout());
        this.area = area;
        this.client = client;
        this.client.detailsf = this;
        pMouseMove = new Point();
        productInfo = new Product();
        lstProduct = area.getListProducts();
        productColor = new ProductColor();
        colours = productColor.getColours();
        numberDrawer = area.getDrawernumber();
        numbershelf = area.getShelfquantity();
        totalDrawer = numberDrawer * numbershelf;
        numberHeight = area.getShelfheight();
        m_Drawers = new Drawer[numberHeight][totalDrawer];
        shelf = new ArrayList<Drawer>();
        pDrawer = new PointDrawer();
        rButton = new Drawer(120, 5, 70, 20);
        idProduct = new ArrayList<String>();
        initShelf();
        initDrawers();
        this.detailListener = new DetailListener(this, this.area, this.client);
        this.addMouseListener(detailListener);
        this.addMouseMotionListener(detailListener);
        this.addMouseWheelListener(detailListener);
    }
    
    public DetailSurface(int minZoomLevel, int maxZoomLevel, double zoomMultiplicationFactor){
        this.detailListener = new DetailListener(this, this.area, this.client);
        this.addMouseListener(detailListener);
        this.addMouseMotionListener(detailListener);
        this.addMouseWheelListener(detailListener);
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(1000, 600);
    }
    
    private void initShelf(){
        int posX = posXDefault;
        int h = height * numberHeight;
        for(int i = 0; i < totalDrawer; i++){
            if(i % numberDrawer == 0 && i != 0){
                 posX += width;
            }
            Drawer s = new Drawer(posX, posYDefault, width, h);
            shelf.add(s);
            posX += width;
        }
    }
    
    public void initDrawers(){
        int posX = posXDefault;
        int posY = posYDefault;

        for(int i = 0; i < numberHeight; i++ ){
            for(int j = 0; j < totalDrawer; j++){
                m_Drawers[i][j] = null;
                if(j % numberDrawer == 0 && j != 0){
                    posX += width;
                }
                Drawer zrect = new Drawer(posX, posY, width, height);
                m_Drawers[i][j] = zrect;
                posX += width;
            }
            posX = posXDefault;
            posY += height;
        }
        for(int id = 0; id < lstProduct.size(); id++)
        {  
            Product product = lstProduct.get(id);
            int nRow = product.getPosRow() - 1;
            int nCol = product.getPosCol() - 1;
            m_Drawers[nRow][nCol].isHidden = false;
            m_Drawers[nRow][nCol].productCode = id;
            m_Drawers[nRow][nCol].numberDrawer = product.getContainer();
            m_Drawers[nRow][nCol].vtNumberDrawer = 1;// to mau san pham o so 1
            m_Drawers[nRow][nCol].colorCode = product.getColorCode();
            if(product.getContainer() == 2){
                int x1 = m_Drawers[nRow][nCol].x1;
                int y1 = m_Drawers[nRow][nCol].y1;
                nCol++;
                if(nCol % numberDrawer == 0 && nCol != 0){
                    x1 += 2*width;
                }else{
                    x1 += width;
                }
                Drawer framesAll = new Drawer(x1, y1, width, height);
                m_Drawers[nRow][nCol] = framesAll;
                m_Drawers[nRow][nCol].isHidden = false;
                m_Drawers[nRow][nCol].productCode = id;
                m_Drawers[nRow][nCol].numberDrawer = product.getContainer();
                m_Drawers[nRow][nCol].vtNumberDrawer = 1;// to mau san pham o so 1
                m_Drawers[nRow][nCol].vtIJ_Drawer = 1;
                m_Drawers[nRow][nCol].colorCode = product.getColorCode();
            }
        }
    }
    private void doDrawing(Graphics g){
        Graphics2D g2d = (Graphics2D)g;
        AffineTransform aftDefault = g2d.getTransform();
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        rh.put(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHints(rh);

        BasicStroke bs2 = new BasicStroke(1, BasicStroke.CAP_ROUND,BasicStroke.JOIN_MITER);
        g2d.setStroke(bs2);
        g2d.fill(rButton);
        Font currentFont = g2d.getFont();
        Font newFont = currentFont.deriveFont(currentFont.getSize() * 1.5F);
        g2d.setFont(newFont);
        g2d.setColor(Color.WHITE);
        g2d.drawString("Xuất HĐ", 122,22);

        g2d.setColor(Color.BLUE);
        g2d.drawString("Số lượng ("+ idProduct.size() +")", 15,20);
        if (init) {
            // Initialize the viewport by moving the origin to the center of the window,
            // and inverting the y-axis to point upwards.
            init = false;
            g2d.scale(1, 1);
            // Save the viewport to be updated by the ZoomAndPanListener
            detailListener.setCoordTransform(g2d.getTransform());
        } else {
            // Restore the viewport after it was updated by the ZoomAndPanListener
            g2d.setTransform(detailListener.getCoordTransform());
        }
        int posXShelf = posXDefault;
        int posYShelf = posYDefault - 10;
        // create name shelf
        newFont = currentFont.deriveFont(currentFont.getSize() * 2.4F);
        g2d.setFont(newFont);
        g2d.setColor(Color.BLACK);
        for(int ij=1; ij <= numbershelf ; ij++){                       
            g2d.drawString("S"+ij, posXShelf + 5,posYShelf + 5);        
            posXShelf += (numberDrawer*width)+width;
        }
        if(objectMove != null){
            if(objectMove.colorCode != -1){
                g2d.setColor(colours[objectMove.colorCode]);
            }
            g2d.fill(objectMove);
           
            if(objectMove.numberDrawer == 2){
                    g2d.setColor(Color.black);
                    g2d.drawString("X", objectMove.x + 38, objectMove.y + 20);
                }
        }
        for(int i = 0; i < numberHeight; i++){
            for(int j = 0; j < totalDrawer; j++){
                if(m_Drawers[i][j].colorCode != -1){
                    if(m_Drawers[i][j].isHidden == false){
                        if(m_Drawers[i][j].vtIJ_Drawer == 1)
                        {                                                   
                            g2d.setColor(colours[m_Drawers[i][j].colorCode]);
                            g2d.fill(m_Drawers[i][j]);
                            g2d.setColor(Color.black);
                            g2d.drawString("X", m_Drawers[i][j].x + 8, m_Drawers[i][j].y + 20);
                        }else{
                            g2d.setColor(colours[m_Drawers[i][j].colorCode]);
                            g2d.fill(m_Drawers[i][j]);
                        }
                    }
                }
                g2d.setColor(Color.BLACK);
                g2d.draw(m_Drawers[i][j]);
            }
        }
        g2d.setTransform(aftDefault);
        if(isMouseMove){
            drawInfo(g2d);
        }
        //end create name shelf
        g2d.dispose();
    }
    
    private void drawInfo(Graphics2D g2d){
        int x = (int)pMouseMove.getX();
        int y = (int)pMouseMove.getY();
        int w = 220;
        int h = 110;
        Font currentFont = g2d.getFont();
        Font newFont = currentFont.deriveFont(currentFont.getSize() / 2F);
        g2d.setFont(newFont);
        g2d.setColor(Color.WHITE);
        g2d.fillRect(x, y, w, h);
        g2d.setColor(Color.black);
        g2d.drawRect(x, y, w, h);
        g2d.drawString("Mã sản phẩm: " + productInfo.getIDProduct(),x + 5, y + 15);
        g2d.drawString("Tên sản phẩm: " + productInfo.getNameProduct(),x + 5, y + 30);
        g2d.drawString("Giá tiền: " + productInfo.getPrice(), x + 5, y + 45);
        g2d.drawString("Ngày nhập kho: " + productInfo.getDate(), x + 5, y + 60);
        g2d.drawString("Hạn sử dụng: " + productInfo.getEndDay(), x + 5, y + 75);
        g2d.drawString("Kích thước: " + productInfo.getContainer(), x + 5, y + 90);
        g2d.drawString("Mã màu: " + productInfo.getColorCode(), x + 5, y + 105);
    }
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        doDrawing(g);
    }
}
