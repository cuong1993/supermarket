/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import form.InfoTransaction;
import form.Supermarket;
import surface.Surface;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.SupermarketArea;
import form.frmMain;
import models.Product;
import surface.DetailSurface;

/**
 *
 * @author Cao
 */
public class Client extends Thread{
  public ArrayList<SupermarketArea> lstArea = null;
// The client socket
  private static Socket clientSocket = null;
  // The output stream
  private static ObjectOutputStream os = null;
  // The input stream
  private static ObjectInputStream is = null;

  private static BufferedReader inputLine = null;
  private static boolean closed = false;
  public Component targetComponent;
  public Surface sf = null;
  public DetailSurface detailsf = null;
  
  public Client(String ip, int port){
    this.lstArea = new ArrayList<SupermarketArea>();
    int portNumber = port;
    String host = ip;
    /*
     * Open a socket on a given host and port. Open input and output streams.
     */
    try {
      clientSocket = new Socket(host, portNumber);
      inputLine = new BufferedReader(new InputStreamReader(System.in));
      os = new ObjectOutputStream(clientSocket.getOutputStream());
      is = new ObjectInputStream(clientSocket.getInputStream());
    } catch (UnknownHostException e) {
      System.err.println("Don't know about host " + host);
    } catch (IOException e) {
      System.err.println("Couldn't get I/O for the connection to the host "
          + host);
    }
  }
  public void close()
  {
      try {
          os.close();
          is.close();
          clientSocket.close();
      } catch (IOException ex) {
          Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
      }
  }
  public void sendObject(Object obj)
  {
      try {
          os.writeObject(obj);
          os.reset();
      } catch (IOException ex) {
          Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
      }
  }
    /*
   * Create a thread to read from the server. (non-Javadoc)
   * 
   * @see java.lang.Runnable#run()
   */
  public void run() {
    /*
     * Keep on reading from the socket till we receive "Bye" from the
     * server. Once we received that then we want to break.
     */
    String responseLine;
    try {
      while ((responseLine = (String)is.readObject()) != null) {
          if (responseLine.equals("bye")){
                break;
          }
          if(responseLine.equals("area_info"))
          {
              lstArea = (ArrayList<SupermarketArea>)is.readObject();
              if(frmMain.isFinish)
              {
                  sf.areas = lstArea;
                  sf.setValue();
                  sf.repaint();
                  if(detailsf != null)
                  {
                      ArrayList<Product> lstProduct = null;
                      SupermarketArea area = sf.areas.get(Surface.posDrawer);
                      lstProduct = area.getListProducts();
                      detailsf.setArea(area);
                      detailsf.setLstProduct(lstProduct);
                      detailsf.initDrawers();
                      detailsf.repaint();
                  }
              }
              frmMain.isFinish = true;
          }
          else if(responseLine.equals("Frequent"))
          {
              InfoTransaction.lstContent = (ArrayList<String>)is.readObject();
              Supermarket.isFinish = true;
          }
          else if(responseLine.equals("Associations"))
          {
              InfoTransaction.lstContent = (ArrayList<String>)is.readObject();
              Supermarket.isFinish = true;
          }
      }
      close();
      closed = true;
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException ex) {
          Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
      }
  }
}
