/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listener;

/**
 *
 * @author Cao
 */

import surface.DetailSurface;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import models.SupermarketArea;
import models.Drawer;
import models.PointDrawer;
import surface.Surface;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;
import models.Product;
import models.UpdateInfo;
import network.Client;
import form.DialogProduct;
import javax.swing.JOptionPane;
import static utils.Constants.*;


/**
 * Listener that can be attached to a Component to implement Zoom and Pan functionality.
 *
 * @author Sorin Postelnicu
 * @since Jul 14, 2009
 */
public class DetailListener implements MouseListener, MouseMotionListener, MouseWheelListener {
    public static final int DEFAULT_MIN_ZOOM_LEVEL = -20;
    public static final int DEFAULT_MAX_ZOOM_LEVEL = 10;
    public static final double DEFAULT_ZOOM_MULTIPLICATION_FACTOR = 1.2;

    private Component targetComponent;
    private AffineTransform inverseScale;

    private int zoomLevel = 0;
    private int minZoomLevel = DEFAULT_MIN_ZOOM_LEVEL;
    private int maxZoomLevel = DEFAULT_MAX_ZOOM_LEVEL;
    private double zoomMultiplicationFactor = DEFAULT_ZOOM_MULTIPLICATION_FACTOR;
    
    private Point dragStartScreen;
    private Point dragEndScreen;
    private AffineTransform coordTransform = new AffineTransform();
    
    private boolean isLeftMouse = false;
    private SupermarketArea area = null;
    private ArrayList<Product> lstProducts = null;
    private Client client = null;
    Point2D mouseLocationPos;
    
    boolean isDrag = false;
    boolean flagDrawerProduct = false;// flag de kiem tra la o keo vao co trung voi o da duoc to may hay ko
    boolean isFlagPopup = false;
    private JPopupMenu popup ;
    PointDrawer startPoint = null;
    private boolean isMoveCamera = false;
    
    public DetailListener(Component targetComponent, SupermarketArea area, Client client) {
        this.targetComponent = targetComponent;
        this.area = area;
        this.client = client;
        popup = new JPopupMenu(); 
        lstProducts = area.getListProducts();
        try {
            inverseScale = coordTransform.createInverse();
        } catch (NoninvertibleTransformException ex) {

        }
    }
    
    public DetailListener(Component targetComponent, int minZoomLevel, int maxZoomLevel, double zoomMultiplicationFactor) {
        this.targetComponent = targetComponent;
        this.minZoomLevel = minZoomLevel;
        this.maxZoomLevel = maxZoomLevel;
        this.zoomMultiplicationFactor = zoomMultiplicationFactor;
        try {
            inverseScale = coordTransform.createInverse();
        } catch (NoninvertibleTransformException ex) {

        }
    }
    private PointDrawer getPosMatrix(Point2D posClick){
        PointDrawer diem = new PointDrawer();
        int col = -1;
        int row = -1;
        for(int i = 0; i < DetailSurface.shelf.size(); i++){
            Drawer dr = DetailSurface.shelf.get(i);
            if(dr.isHit((float)posClick.getX(), (float)posClick.getY())){
                col = i;
                break;
            }
        }
        int posx = (int)posClick.getY();
        if(col != -1){
            row = (posx - 50) / 20;
            if(row == this.area.getShelfheight()){
                row = -1;
            }
        }
        DetailSurface.pDrawer.row = row;
        DetailSurface.pDrawer.col = col;
        diem.row = row;
        diem.col = col;
        return diem;
    }
    public void mousePressed(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1){
            DetailSurface.isMouseMove = false;
            isFlagPopup = false;
            isDrag = true;
            isLeftMouse = true;
            mouseLocationPos = e.getPoint();
            inverseScale.transform(mouseLocationPos, mouseLocationPos);
            startPoint = getPosMatrix(mouseLocationPos);
            int row = -1;
            int col = -1;
            row = startPoint.row;
            col = startPoint.col;
            if(DetailSurface.rButton.isHit((float)mouseLocationPos.getX(), (float)mouseLocationPos.getY()))
            {
                this.client.sendObject("hd");
                this.client.sendObject(DetailSurface.idProduct);
                DetailSurface.idProduct.removeAll(DetailSurface.idProduct);
                return;
            }
            if(row != -1 && col != -1){
                if(DetailSurface.m_Drawers[row][col].vtIJ_Drawer == 1){
                    startPoint.row = -1;
                    startPoint.col = -1;
                    return;
                }
                DetailSurface.objectMove = new Drawer((int)DetailSurface.m_Drawers[row][col].getX(), 
                        (int)DetailSurface.m_Drawers[row][col].getY(), 
                        (int)(DetailSurface.m_Drawers[row][col].getWidth() * DetailSurface.m_Drawers[row][col].numberDrawer),
                        (int)DetailSurface.m_Drawers[row][col].getHeight());
                DetailSurface.objectMove.colorCode = DetailSurface.m_Drawers[row][col].colorCode;
                DetailSurface.objectMove.numberDrawer = DetailSurface.m_Drawers[row][col].numberDrawer;
                DetailSurface.m_Drawers[row][col].isHidden = true;
                startPoint.color = DetailSurface.m_Drawers[row][col].colorCode;
                if(DetailSurface.m_Drawers[row][col].numberDrawer == 2){
                    DetailSurface.m_Drawers[row][col + 1].isHidden = true;
                }
            }           
            targetComponent.repaint();          
        }
        if(e.getButton() == MouseEvent.BUTTON3){
            DetailSurface.isMouseMove = false;
            isFlagPopup = false;
            popup.setVisible(false);
            targetComponent.repaint();
            mouseLocationPos = e.getPoint();
            inverseScale.transform(mouseLocationPos, mouseLocationPos);           
            PointDrawer stPos = getPosMatrix(mouseLocationPos);
            if(stPos.row != -1 && stPos.col != -1){
                if(DetailSurface.m_Drawers[stPos.row][stPos.col].isHit((float)mouseLocationPos.getX(),(float)mouseLocationPos.getY())){
                    startPoint = getPosMatrix(mouseLocationPos);
                    int pos = DetailSurface.m_Drawers[stPos.row][stPos.col].productCode;
                    if(pos == -1){
                        isFlagPopup = false;
                    }else{
                        isFlagPopup = true;
                        startPoint.col = -1;
                        startPoint.row = -1;
                    }
                    if(DetailSurface.m_Drawers[stPos.row][stPos.col].vtIJ_Drawer != 1)
                    {
                        menuPopup(e,isFlagPopup,stPos);
                        targetComponent.repaint();
                    }
                }
            }else{
                isMoveCamera = true;
                dragStartScreen = e.getPoint();
                startPoint = getPosMatrix(mouseLocationPos);
                startPoint.col = -1;
                startPoint.row = -1;
            }
        }
    }
   
    public void mouseDragged(MouseEvent e) {// click mouse and drag
        if(isLeftMouse){
            isFlagPopup = false;
            if(isDrag){
                doMove(e);
            } 
        }
        else{
            if(isMoveCamera){
                moveCamera(e);
            }
        }
    }
    
    private boolean isChange(PointDrawer endPoint){
        int row = endPoint.row;
        int col = endPoint.col;
        if(DetailSurface.m_Drawers[startPoint.row][startPoint.col].numberDrawer == 1){
            return true;
        }
        int nColLimit = this.area.getShelfquantity() * this.area.getDrawernumber();
        if(col + 1 == nColLimit){
            return false;
        }
        if(DetailSurface.m_Drawers[startPoint.row][startPoint.col].numberDrawer == 2){
            if(DetailSurface.m_Drawers[row][col + 1].productCode == -1){
                return true;
            }
        }
        return false;
    }
    
    public void mouseReleased(MouseEvent e) {
        isMoveCamera = false;
        isLeftMouse = false;
        mouseLocationPos = e.getPoint();
        inverseScale.transform(mouseLocationPos, mouseLocationPos);
        PointDrawer endPoint = getPosMatrix(mouseLocationPos);
        int row = -1;
        int col = -1;
        row = endPoint.row;
        col = endPoint.col;
        if(startPoint.row != -1 && startPoint.col != -1){
            DetailSurface.m_Drawers[startPoint.row][startPoint.col].isHidden = false;
              if(DetailSurface.m_Drawers[startPoint.row][startPoint.col].numberDrawer == 2){
                  DetailSurface.m_Drawers[startPoint.row][startPoint.col + 1].isHidden = false;
              }
            if(row != -1 && col != -1){
              if(isChange(endPoint)){
                  if(DetailSurface.m_Drawers[row][col].productCode == -1){
                  DetailSurface.m_Drawers[row][col].CopyAtoB(DetailSurface.m_Drawers[startPoint.row][startPoint.col]);
                  Product product1 = lstProducts.get(DetailSurface.m_Drawers[row][col].productCode);
                  product1.setPosRow(row+1);
                  product1.setPosCol(col + 1);
                  DetailSurface.m_Drawers[startPoint.row][startPoint.col].setDefaultDrawer();
                  if(DetailSurface.m_Drawers[row][col].numberDrawer == 2){
                        if(!(row == startPoint.row && col == startPoint.col)){
                            DetailSurface.m_Drawers[row][col+1].CopyAtoB(DetailSurface.m_Drawers[startPoint.row][startPoint.col+1]);
                            Product product2 = lstProducts.get(DetailSurface.m_Drawers[row][col].productCode);
                            product2.setPosRow(row+1);
                            product2.setPosCol(col+1);
                            DetailSurface.m_Drawers[startPoint.row][startPoint.col+1].setDefaultDrawer();
                        }
                    }
                      this.client.sendObject("update");
                      UpdateInfo info = new UpdateInfo(Surface.posDrawer, this.area);
                      this.client.sendObject(info);
              }
              AreaListener.targetComponent.repaint();
            } 
            startPoint.row = -1;
            startPoint.col = -1;
        }
        }
        if(DetailSurface.objectMove != null){
            DetailSurface.objectMove.width = 0;
            DetailSurface.objectMove.height = 0;
        }
        
        targetComponent.repaint();
        DetailSurface.objectMove = null;
    }      
    private int FindProduct(ArrayList<Product> listProducts,PointDrawer stPos)
    {
        for(int i=0; i<listProducts.size();i++){
            if((listProducts.get(i).getPosRow()-1) == stPos.row && (listProducts.get(i).getPosCol()-1) == stPos.col){
                return i;                       
            }
        }
        return -1;
    }
    public void menuPopup(MouseEvent e, Boolean isFlagPopup,PointDrawer stPos){
        //create actionlistener
        ActionListener menuListener = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                if(event.getActionCommand() == "Thêm"){
                targetComponent.repaint();
                DialogProduct dgProduct = new DialogProduct(area,stPos,client,ADD_PRODUCT,-1);
                dgProduct.setVisible(true);
                targetComponent.repaint();
              }
              else if(event.getActionCommand().equals("Sửa")){              
                targetComponent.repaint();
                int posProduct = FindProduct(lstProducts, stPos);
                DialogProduct dgProduct = new DialogProduct(area,stPos,client,EDIT_PRODUCT,posProduct);
                dgProduct.setVisible(true);
                targetComponent.repaint();
              }
              else if(event.getActionCommand().equals("Xóa")){
                targetComponent.repaint();
                int option = JOptionPane.showOptionDialog(null,"Bạn có thực sự muốn xóa sản phẩm này?" ,
                        "Thông bao", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE,null, null, null);
                if(option == JOptionPane.CANCEL_OPTION){
                    return;
                }
                int posProduct = FindProduct(lstProducts, stPos);
                if(posProduct != -1)
                {
                    lstProducts.remove(posProduct);
                    client.sendObject("update");
                    UpdateInfo info = new UpdateInfo(Surface.posDrawer, area);
                    client.sendObject(info);
                }
                targetComponent.repaint();
              }
              else if(event.getActionCommand().equals("Bán")){
                    targetComponent.repaint();
                    int posProduct = FindProduct(lstProducts, stPos);
                    if(posProduct != -1)
                    {
                        Product p = lstProducts.get(posProduct);
                        DetailSurface.idProduct.add(p.getIDProduct());
                        lstProducts.remove(posProduct);
                        client.sendObject("update");
                        UpdateInfo info = new UpdateInfo(Surface.posDrawer, area);
                        client.sendObject(info);
                    }
                    targetComponent.repaint();
                }
            }
        };
        JMenuItem item;
        popup.removeAll();
        popup.add(item = new JMenuItem("Thêm", new ImageIcon("add.png")));
        item.setHorizontalTextPosition(JMenuItem.RIGHT);
        if(isFlagPopup == true){
            item.setEnabled(false);
        }
        item.addActionListener(menuListener);
        popup.addSeparator();
        popup.add(item = new JMenuItem("Xóa",  new ImageIcon("delete.png")));
        item.setHorizontalTextPosition(JMenuItem.RIGHT);
        if(isFlagPopup == false){
            item.setEnabled(false);
        }
        item.addActionListener(menuListener);
        popup.addSeparator();
        popup.add(item = new JMenuItem("Sửa",  new ImageIcon("update.png")));
        item.setHorizontalTextPosition(JMenuItem.RIGHT);
        if(isFlagPopup == false){
            item.setEnabled(false);
        }
        item.addActionListener(menuListener);
        popup.addSeparator();
        popup.add(item = new JMenuItem("Bán",  new ImageIcon("bag.png")));
        item.setHorizontalTextPosition(JMenuItem.RIGHT);
        if(isFlagPopup == false){
            item.setEnabled(false);
        }
        item.addActionListener(menuListener);
        popup.addSeparator();
        JPanel contentPane = new JPanel();
        //create action
        popup.setLabel("MENU");
        popup.setBorder(new BevelBorder(BevelBorder.RAISED));
        //end action
        contentPane.add(popup);      
        popup.show(e.getComponent(), (int) e.getPoint().getX(),  (int)e.getPoint().getY());
    }
    private void doMove(MouseEvent e) {
        Point2D mouseLocation = e.getPoint();
        inverseScale.transform(mouseLocation, mouseLocation);
        double x = mouseLocationPos.getX();
        double y = mouseLocationPos.getY();
        int dx = (int)(mouseLocation.getX() - x);
        int dy = (int)(mouseLocation.getY() - y);
    
        if(DetailSurface.objectMove != null){
            if(DetailSurface.objectMove.isHit((float)x, (float)y)){
                DetailSurface.objectMove.addX(dx);
                DetailSurface.objectMove.addY(dy);
            }
        }
        targetComponent.repaint();
        mouseLocationPos.setLocation(x+dx, y+dy);       
    }
    public void mouseClicked(MouseEvent e) {

    }
    
    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
        
    }

    public void mouseMoved(MouseEvent e) {
        DetailSurface.pMouseMove.setLocation(e.getX(), e.getY());
        mouseLocationPos = e.getPoint();
        inverseScale.transform(mouseLocationPos, mouseLocationPos);
        PointDrawer stPos = getPosMatrix(mouseLocationPos);
        if(stPos.row != -1 && stPos.col != -1){
            if(DetailSurface.m_Drawers[stPos.row][stPos.col].isHit((float)mouseLocationPos.getX(),(float)mouseLocationPos.getY())){
                int posProduct = FindProduct(lstProducts, stPos);
                if(posProduct != -1)
                {
                    Product product = lstProducts.get(posProduct);
                    DetailSurface.isMouseMove = true;
                    DetailSurface.productInfo.setIDProduct(product.getIDProduct());
                    DetailSurface.productInfo.setNameProduct(product.getNameProduct());
                    DetailSurface.productInfo.setPrice(product.getPrice());
                    DetailSurface.productInfo.setDate(product.getDate());
                    DetailSurface.productInfo.setEndDay(product.getEndDay());
                    DetailSurface.productInfo.setContainer(product.getContainer());
                    DetailSurface.productInfo.setColorCode(product.getColorCode());
                }
                else
                {
                    DetailSurface.isMouseMove = false;
                }
            }
            else
            {
                DetailSurface.isMouseMove = false;
            }
        }
        else
        {
            DetailSurface.isMouseMove = false;
        }
        targetComponent.repaint();
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
        zoomCamera(e);
    }        

    private void moveCamera(MouseEvent e) {
        try {
            dragEndScreen = e.getPoint();
            Point2D.Float dragStart = transformPoint(dragStartScreen);
            Point2D.Float dragEnd = transformPoint(dragEndScreen);
            double dx = dragEnd.getX() - dragStart.getX();
            double dy = dragEnd.getY() - dragStart.getY();
            coordTransform.translate(dx, dy);
            dragStartScreen = dragEndScreen;
            dragEndScreen = null;
            try {
                inverseScale = coordTransform.createInverse();
            } catch (NoninvertibleTransformException ex) {

            }
            targetComponent.repaint();
        } catch (NoninvertibleTransformException ex) {
            ex.printStackTrace();
        }
    }

    private void zoomCamera(MouseWheelEvent e) {
        try {
            int wheelRotation = e.getWheelRotation();
            Point p = e.getPoint();
            if (wheelRotation > 0) {
                if (zoomLevel < maxZoomLevel) {
                    zoomLevel++;
                    Point2D p1 = transformPoint(p);
                    coordTransform.scale(1 / zoomMultiplicationFactor, 1 / zoomMultiplicationFactor);
                    Point2D p2 = transformPoint(p);
                    coordTransform.translate(p2.getX() - p1.getX(), p2.getY() - p1.getY());
                    try {
                        inverseScale = coordTransform.createInverse();
                    } catch (NoninvertibleTransformException ex) {

                    }
                    targetComponent.repaint();
                }
            } else {
                if (zoomLevel > minZoomLevel) {
                    zoomLevel--;
                    Point2D p1 = transformPoint(p);
                    coordTransform.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
                    Point2D p2 = transformPoint(p);
                    coordTransform.translate(p2.getX() - p1.getX(), p2.getY() - p1.getY());
                    try {
                        inverseScale = coordTransform.createInverse();
                    } catch (NoninvertibleTransformException ex) {

                    }
                    targetComponent.repaint();
                }
            }
        } catch (NoninvertibleTransformException ex) {
            ex.printStackTrace();
        }
    }

    private Point2D.Float transformPoint(Point p1) throws NoninvertibleTransformException {
        AffineTransform inverse = coordTransform.createInverse();

        Point2D.Float p2 = new Point2D.Float();
        inverse.transform(p1, p2);
        return p2;
    }

    public int getZoomLevel() {
        return zoomLevel;
    }

    public void setZoomLevel(int zoomLevel) {
        this.zoomLevel = zoomLevel;
    }
    
    public AffineTransform getCoordTransform() {
        return coordTransform;
    }
    
    public void setCoordTransform(AffineTransform coordTransform) {
        this.coordTransform = coordTransform;
    }
}
