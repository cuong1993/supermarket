/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listener;

/**
 *
 * @author Cao
 */
import static utils.Utils.distanceBetween;
import surface.Surface;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import models.SupermarketArea;
import form.AreaDetail;

/**
 * Listener that can be attached to a Component to implement Zoom and Pan functionality.
 *
 * @author Sorin Postelnicu
 * @since Jul 14, 2009
 */
public class AreaListener implements MouseListener, MouseMotionListener, MouseWheelListener {
    public static final int DEFAULT_MIN_ZOOM_LEVEL = -20;
    public static final int DEFAULT_MAX_ZOOM_LEVEL = 10;
    public static final double DEFAULT_ZOOM_MULTIPLICATION_FACTOR = 1.2;
    public static Polygon[] polygons;
    public int size = 0;
    public static Component targetComponent;
    private AffineTransform inverseScale;

    private int zoomLevel = 0;
    private int minZoomLevel = DEFAULT_MIN_ZOOM_LEVEL;
    private int maxZoomLevel = DEFAULT_MAX_ZOOM_LEVEL;
    private double zoomMultiplicationFactor = DEFAULT_ZOOM_MULTIPLICATION_FACTOR;
    
    private Point dragStartScreen;
    private Point dragEndScreen;
    private AffineTransform coordTransform = new AffineTransform();

    private boolean isLeftMouse = false;
    private boolean isDragged = false;
    
    private Point startPoint =  null;
    private Point endPoint = null;
    private Point2D startMouse = null;
    private Point2D endMouse = null;
    Rectangle2D rect = null;
    
    private float[] distances = null;
    private ArrayList<SupermarketArea> areas = null;
    
    public AreaListener(Component targetComponent) {
        this.targetComponent = targetComponent;
        this.startPoint = new Point(0,0);
        this.endPoint = new Point(0, 0);
        try {
            inverseScale = coordTransform.createInverse();
        } catch (NoninvertibleTransformException ex) {

        }
    }

    public AreaListener(Component targetComponent, int minZoomLevel, int maxZoomLevel, double zoomMultiplicationFactor) {
        this.targetComponent = targetComponent;
        this.minZoomLevel = minZoomLevel;
        this.maxZoomLevel = maxZoomLevel;
        this.zoomMultiplicationFactor = zoomMultiplicationFactor;
        this.startPoint = new Point(0,0);
        this.endPoint = new Point(0, 0);
        try {
            inverseScale = coordTransform.createInverse();
        } catch (NoninvertibleTransformException ex) {

        }
    }

    public void createPolygons(int size){
        this.size = size;
        polygons = new Polygon[size];// create array polygon
        distances = new float[size];
    }
    
    public void setStartPoint(Point startpoint) {
        Surface.xChoosed = startpoint.x;
        Surface.yChoosed = startpoint.y;
    }

    public void setEndPoint(Point endpoint) {
        Surface.wChoosed = (endpoint.x);
        Surface.hChoosed = (endpoint.y);
    }
    
    public void mouseClicked(MouseEvent e) {       
    }
    
    public void mousePressed(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1){
            isLeftMouse = true;
            Surface.isDrawMouseArea = true;
            Point2D mouseLocation = e.getPoint();
            inverseScale.transform(mouseLocation, mouseLocation);// chuyen doi toa do mouse sang toa do hinh
            this.startMouse = mouseLocation;
            this.startPoint = e.getPoint();
            setStartPoint(this.startPoint);
            for(int i = 0; i < size; i++){
                if(polygons[i].contains(mouseLocation)){// kiem tra xem toa do x,y co nam trong hinh hay ko
                    //contains polygon
                    Surface.pos = i;// return value image clicked
                }
            }
        }
        if(e.getButton() == MouseEvent.BUTTON3){
            dragStartScreen = e.getPoint();
            dragEndScreen = null;
        }
    }

    public void mouseReleased(MouseEvent e) {//da nhan vao hinh va tha mouse
        isLeftMouse = false;
        Surface.isDrawMouseArea = false;
        Surface.xChoosed = 0;
        Surface.yChoosed = 0;
        Surface.wChoosed = 0;
        Surface.hChoosed = 0;
        if(isDragged){
            this.isDragged = false;
            chooseArea((int)startMouse.getX(), (int)startMouse.getY(), (int)endMouse.getX(), (int)endMouse.getY());
            initDistance();
            Surface.pos = nearTopLeftCorner();// tra ra vi tri mouse keo gan nhat
            Surface.posDrawer = Surface.pos;
        }
        targetComponent.repaint();
        if(Surface.pos != -1){
            Surface.posDrawer = Surface.pos;
            AreaDetail areaDetail = new AreaDetail(areas.get(Surface.pos), Surface.client);
            areaDetail.setVisible(true);
        }
        targetComponent.repaint();
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
        if(isLeftMouse){
            this.isDragged = true;
            Point2D mouseLocation = e.getPoint();
            inverseScale.transform(mouseLocation, mouseLocation);
            this.endMouse = mouseLocation;
            this.endPoint = e.getPoint();
            setEndPoint(this.endPoint);
            targetComponent.repaint(); 
         }
        else{
            moveCamera(e);
        }
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
        zoomCamera(e);
    }
    
    private void chooseArea(int x, int y, int x2, int y2){
        int px = Math.min(x,x2);
        int py = Math.min(y,y2);
        int pw = Math.abs(x-x2);
        int ph = Math.abs(y-y2);
        rect = new Rectangle2D.Float(px, py, pw, ph);
    }
    
    private int nearTopLeftCorner(){
        int pos = 0;
        boolean isIntersects = false;
        for(int i = 0; i < size; i++){
            if(polygons[i].intersects(rect)){
                pos = i;
                isIntersects = true;
                break;
            }
        }
        if(isIntersects){
            for(int j = pos + 1; j < size; j++){
                if(polygons[j].intersects(rect)){
                    if(distances[j] < distances[pos]){
                        pos = j;
                    }
                }
            }
        }else{
            pos = -1;
        }
        return pos;
    }
    
    private void initDistance(){
        int way = movedWay();
        for(int i = 0; i < size; i++){
           distances[i] = distanceTopLeftCorner(way,i); 
        }
    }
    
    private float distanceTopLeftCorner(int way, int pos){
        float distance = 0;
        switch(way){
            case 1:
                Point p1 = new Point((int)startMouse.getX(), (int)endMouse.getY());
                distance = distanceBetween(p1, polygons[pos] );
                break;
            case 2:
                Point p2 = new Point((int)startMouse.getX(), (int)startMouse.getY());
                distance = distanceBetween(p2, polygons[pos] );
                break;
            case 3:
                Point p3 = new Point((int)endMouse.getX(), (int)startMouse.getY());
                distance = distanceBetween(p3, polygons[pos]);
                break;
            case 4:
                Point p4 = new Point((int)endMouse.getX(), (int)endMouse.getY());
                distance = distanceBetween(p4, polygons[pos]);
                break;
            default:
                distance = 0;
                break;
        }
        return distance;
    }
    
    private int movedWay(){
        if(endPoint.x > startPoint.x && endPoint.y < startPoint.y){
            return 1;
        }
        if(endPoint.x > startPoint.x && endPoint.y > startPoint.y){
            return 2;
        }
        if(endPoint.x < startPoint.x && endPoint.y > startPoint.y){
            return 3;
        }
        return 4;
    }
    
    private void moveCamera(MouseEvent e) {
        try {
            dragEndScreen = e.getPoint();
            Point2D.Float dragStart = transformPoint(dragStartScreen);
            Point2D.Float dragEnd = transformPoint(dragEndScreen);
            double dx = dragEnd.getX() - dragStart.getX();
            double dy = dragEnd.getY() - dragStart.getY();
            coordTransform.translate(dx, dy);
            dragStartScreen = dragEndScreen;
            dragEndScreen = null;
            try {
                inverseScale = coordTransform.createInverse();
            } catch (NoninvertibleTransformException ex) {

            }
            targetComponent.repaint();
        } catch (NoninvertibleTransformException ex) {
            ex.printStackTrace();
        }
    }

    private void zoomCamera(MouseWheelEvent e) {
        try {
            int wheelRotation = e.getWheelRotation();
            Point p = e.getPoint();
            if (wheelRotation > 0) {
                if (zoomLevel < maxZoomLevel) {
                    zoomLevel++;
                    Point2D p1 = transformPoint(p);
                    coordTransform.scale(1 / zoomMultiplicationFactor, 1 / zoomMultiplicationFactor);
                    Point2D p2 = transformPoint(p);
                    coordTransform.translate(p2.getX() - p1.getX(), p2.getY() - p1.getY());
                    try {
                        inverseScale = coordTransform.createInverse();
                    } catch (NoninvertibleTransformException ex) {

                    }
                    targetComponent.repaint();
                }
            } else {
                if (zoomLevel > minZoomLevel) {
                    zoomLevel--;
                    Point2D p1 = transformPoint(p);
                    coordTransform.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
                    Point2D p2 = transformPoint(p);
                    coordTransform.translate(p2.getX() - p1.getX(), p2.getY() - p1.getY());
                    try {
                        inverseScale = coordTransform.createInverse();
                    } catch (NoninvertibleTransformException ex) {

                    }
                    targetComponent.repaint();
                }
            }
        } catch (NoninvertibleTransformException ex) {
            ex.printStackTrace();
        }
    }

    private Point2D.Float transformPoint(Point p1) throws NoninvertibleTransformException {
        AffineTransform inverse = coordTransform.createInverse();

        Point2D.Float p2 = new Point2D.Float();
        inverse.transform(p1, p2);
        return p2;
    }

    public int getZoomLevel() {
        return zoomLevel;
    }

    public void setZoomLevel(int zoomLevel) {
        this.zoomLevel = zoomLevel;
    }
    
    public AffineTransform getCoordTransform() {
        return coordTransform;
    }
    
    public void setCoordTransform(AffineTransform coordTransform) {
        this.coordTransform = coordTransform;
    }

    public ArrayList<SupermarketArea> getAreas() {
        return areas;
    }

    public void setAreas(ArrayList<SupermarketArea> areas) {
        this.areas = areas;
    }
}