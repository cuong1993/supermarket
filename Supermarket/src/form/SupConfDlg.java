/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Cao
 */
public class SupConfDlg extends JDialog{
    private JTextField txtMinconf = null;
    private JTextField txtMinsup= null;
    private JButton btnAdd = null;
    public static boolean isOK = false;
    public static double minsup = 0.0;
    public static double minconf = 0.0;
    public SupConfDlg(){
        InitUI();
    }
    private void InitUI()
    {
        setModal(true);
        setResizable(false);
        setTitle("Minsup & Minconf");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setSize(235, 135);
        setLayout(null);
        setResizable(false);
        setLocationRelativeTo(null);
        
        JLabel lblMinsup = new JLabel("Minsup:");
        lblMinsup.setBounds(12, 20, 95, 20);
        add(lblMinsup);
        
        txtMinsup= new JTextField();
        txtMinsup.setBounds(55, 20, 160, 20);
        txtMinsup.setText("");
        txtMinsup.setEnabled(true);
        add(txtMinsup); 
        
        JLabel lblMinconf = new JLabel("Minconf:");
        lblMinconf.setBounds(12, 50, 42, 20);
        add(lblMinconf);
        
        txtMinconf = new JTextField();
        txtMinconf.setBounds(55, 50, 160, 20);
        txtMinconf.setText("");
        txtMinconf.setEnabled(true);
        add(txtMinconf);
        
        btnAdd = new JButton("OK");
        btnAdd.setBounds(155, 75, 60, 25);
        btnAdd.addActionListener(new menuListener());
        add(btnAdd); 
    }
    private boolean isNumber(String text){
        try {
            double number = Double.parseDouble(text);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    private boolean isAllowed()
    {
        if(txtMinsup.getText().trim().isEmpty())
        {
            return false;
        }
        if(!isNumber(txtMinsup.getText().trim()))
        {
            return false;
        }
        if(txtMinconf.getText().trim().isEmpty())
        {
            return false;
        }
        if(!isNumber(txtMinconf.getText().trim()))
        {
            return false;
        }
        return true;
    }
    public class menuListener implements ActionListener{
        
        @Override
        public void actionPerformed(ActionEvent e){
            if(isAllowed())
            {
                minsup = Double.parseDouble(txtMinsup.getText().trim());
                minconf = Double.parseDouble(txtMinconf.getText().trim());
                isOK = true;
                dispose();
            }
        }
    };
}
