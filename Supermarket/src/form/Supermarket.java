/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import surface.Surface;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import models.SupermarketArea;
import network.Client;

/**
 *
 * @author Cao
 */
public class Supermarket extends JFrame {
    private ArrayList<SupermarketArea> lstArea = null;
    private Client client = null;
    public static boolean isFinish = false;
    public Supermarket(ArrayList<SupermarketArea> lstArea, Client client){
        this.lstArea = lstArea;
        this.client = client;
        initUI();
    }
    private void initUI(){
        //create menu
        setSize(150, 150); 
        // Creates a menubar for a JFrame
        JMenuBar menuBar = new JMenuBar();
        // Add the menubar to the frame
        // Define and add two drop down menu to the menubar
        JMenu fileMenu = new JMenu("File");
        menuBar.add(fileMenu);
         
        // Create and add simple menu item to one of the drop down menu
        JMenuItem newAction = new JMenuItem("Frequent Patterns");
        JMenuItem associations = new JMenuItem("Associations");
        fileMenu.add(newAction);
        fileMenu.add(associations);
        fileMenu.addSeparator();
        newAction.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SupConfDlg dlg = new SupConfDlg();
                dlg.setVisible(true);
                if(!SupConfDlg.isOK)
                    return;
                SupConfDlg.isOK = false;
                client.sendObject("Frequent");
                String sMin = String.valueOf(SupConfDlg.minsup) + ",";
                sMin += String.valueOf(SupConfDlg.minconf);
                client.sendObject(sMin);
                while(!isFinish)
                {
                    System.out.print("");
                }
                InfoTransaction info = new InfoTransaction();
                info.setVisible(true);
                isFinish = false;
            }
        });
        associations.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SupConfDlg dlg = new SupConfDlg();
                dlg.setVisible(true);
                if(!SupConfDlg.isOK)
                    return;
                SupConfDlg.isOK = false;
                client.sendObject("Associations");
                String sMin = String.valueOf(SupConfDlg.minsup) + ",";
                sMin += String.valueOf(SupConfDlg.minconf);
                client.sendObject(sMin);
                while(!isFinish)
                {
                    System.out.print("");
                }
                InfoTransaction info = new InfoTransaction();
                info.setVisible(true);
                isFinish = false;
            }
        });
        // End create menu
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                client.sendObject("/quit");
                client.stop();
                System.exit(0);
            }
        });
        setTitle("Supermarket");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        Surface sf = new Surface(this.lstArea, this.client);
        sf.setLayout(new BorderLayout());
        sf.add(menuBar, BorderLayout.NORTH);
        add(sf, BorderLayout.CENTER);
        
        Dimension DimMax = Toolkit.getDefaultToolkit().getScreenSize();  
        setSize(DimMax.width, DimMax.height);
        setResizable(false);
        setLocationRelativeTo(null);
    }
}
