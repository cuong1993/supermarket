/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import models.SupermarketArea;
import network.Client;

/**
 *
 * @author Cao
 */
public class frmMain extends JFrame{
    private Client client = null;
    private ArrayList<SupermarketArea> lstArea = null;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        EventQueue.invokeLater(new Runnable(){
            @Override
            public void run(){
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                    frmMain frm = new frmMain();
                    frm.setVisible(true);
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                    Logger.getLogger(Supermarket.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    public static boolean isFinish = false;
    public frmMain()
    {
        setTitle("Supermarket");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(295, 125);
        setLayout(null);
        setResizable(false);
        setLocationRelativeTo(null);
        
        JLabel lblServer = new JLabel("Server IP: ");
        lblServer.setBounds(10, 14, 52, 14);
        add(lblServer);
        
        JTextField txtIP = new JTextField();
        txtIP.setBounds(66, 11, 210, 20);
        add(txtIP);
        
        JLabel lblPort = new JLabel("Port:");
        lblPort.setBounds(10, 40, 24, 14);
        add(lblPort);
        
        JTextField txtPort = new JTextField();
        txtPort.setBounds(66, 37, 210, 20);
        add(txtPort);
        
        txtIP.setText("127.0.0.1");
        txtPort.setText("2222");
        
        JButton btnConnect = new JButton();
        btnConnect.setText("Connect");
        btnConnect.setName("btnConnect"); // NOI18N
        btnConnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setVisible(false);
                String ip = txtIP.getText().trim();
                int port = Integer.parseInt(txtPort.getText().trim());
                client = new Client(ip, port);
                client.start();
                while(!isFinish)
                {
                    System.out.print("");
                }
                lstArea = client.lstArea;
                Supermarket supermarket = new Supermarket(lstArea, client);
                supermarket.setVisible(true);
            }
        });
        getContentPane().add(btnConnect);
        btnConnect.setBounds(203, 63, 73, 23);
    }
}
