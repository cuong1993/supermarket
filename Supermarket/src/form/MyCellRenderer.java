/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

class MyCellRenderer extends JLabel implements ListCellRenderer {
    Color[] colors=null;
    boolean b=false;
     public MyCellRenderer(Color[] colors) {  
         this.colors = colors;
         setOpaque(true); 
     }
    @Override
    public void setBackground(Color bg) {
        // TODO Auto-generated method stub
         if(!b)
         {
             return;
         }
        super.setBackground(bg);
    }
     public Component getListCellRendererComponent(  JList list,  Object value,  int index,boolean isSelected,boolean cellHasFocus)  
     {  
         b=true;
         setText(value.toString());           
         setBackground(colors[Integer.parseInt(value.toString())]);        
         b=false;
         return this;  
     }  
}
