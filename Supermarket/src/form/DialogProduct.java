/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import models.PointDrawer;
import models.Product;
import models.SupermarketArea;
import models.UpdateInfo;
import network.Client;
import surface.DetailSurface;
import surface.Surface;
import static utils.Constants.*;
import utils.ProductColor;

/**
 *
 * @author MAN
 */


public class DialogProduct extends JDialog{
    private SupermarketArea area;
    private int posProduct = -1;
    private int row = -1;
    private int col = -1;
    private JButton btnAdd = null;
    private JTextField txtNameProduct = null;
    private JTextField txtMaProduct = null;
    private JComboBox cboSLOKH = null;
    private JComboBox cboMaMau = null;
    private JTextField txtDate = null;
    private JTextField txtPrice = null;
    private JTextField txtHSD = null;
    private ProductColor productColor = null;
    private Color[] colors = null;
    private Client client = null;
    private int option = -1;
    private int typeadd = NOT_ADD;
    public DialogProduct(SupermarketArea area, PointDrawer point,Client client, int option, int posProduct){
        this.area = area;
        this.row = point.row;
        this.col = point.col;
        this.client = client;      
        this.option = option;
        this.posProduct = posProduct;
        productColor = new ProductColor();
        colors = productColor.getColours();
        initUI();
    }
    private void initUI(){
        setModal(true);
        setResizable(false);
        String title = "";
        if(this.option == ADD_PRODUCT)
        {
            title = "Thêm";
        }
        else if(this.option == EDIT_PRODUCT)
        {
            title = "Sửa";
        }
        setTitle(title+" Sản Phẩm");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setSize(340, 285);
        setLayout(null);
        setResizable(false);
        setLocationRelativeTo(null);
        
        JLabel lblMaProduct = new JLabel("Mã sản phẩm:");
        lblMaProduct.setBounds(12, 20, 95, 20);
        add(lblMaProduct);
        
        txtMaProduct= new JTextField();
        txtMaProduct.setBounds(100, 20, 220, 20);
        txtMaProduct.setText("");
        txtMaProduct.setEnabled(true);
        add(txtMaProduct); 
        
        JLabel lblNameProduct = new JLabel("Tên sản phẩm:");
        lblNameProduct.setBounds(12, 50, 95, 20);
        add(lblNameProduct);
        
        txtNameProduct = new JTextField();
        txtNameProduct.setBounds(100, 50, 220, 20);
        txtNameProduct.setText("");
        txtNameProduct.setEnabled(true);
        add(txtNameProduct);    
        
        JLabel lblPrice = new JLabel("Giá tiền:");
        lblPrice.setBounds(12, 80, 60, 20);
        add(lblPrice);
        
        txtPrice = new JTextField();
        txtPrice.setBounds(100, 80, 220, 20);
        txtPrice.setText("");
        txtPrice.setEnabled(true);
        add(txtPrice);
        
        JLabel lblDate = new JLabel("Ngày nhập kho:");
        lblDate.setBounds(12, 110, 95, 20);
        add(lblDate);
        
        txtDate = new JTextField();
        txtDate.setBounds(100, 110, 220, 20);
        txtDate.setText("");
        txtDate.setEnabled(true);
        add(txtDate);
        
        
        JLabel lblHSD= new JLabel("Hạn sử dụng:");
        lblHSD.setBounds(12, 140, 96, 20);
        add(lblHSD);
        
        txtHSD = new JTextField();
        txtHSD.setBounds(100, 140, 220, 20);
        txtHSD.setText("");
        txtHSD.setEnabled(true);
        add(txtHSD);

        JLabel lblSLOKH= new JLabel("Số ô kệ hàng:");
        lblSLOKH.setBounds(12, 170, 96, 20);
        add(lblSLOKH);
        
        String []soluong = {"1","2"};
        cboSLOKH = new JComboBox(soluong);
        cboSLOKH.setBounds(100, 170, 220, 20);
        cboSLOKH.setSelectedIndex(0);
        cboSLOKH.setEnabled(true);
        add(cboSLOKH);
        
        JLabel lblMaMau = new JLabel("Mã màu:");
        lblMaMau.setBounds(12, 200, 60, 20);
        add(lblMaMau);
        
        String []sColor = new String[colors.length];
        for(int i = 0; i < colors.length; i++)
        {
            sColor[i] = String.valueOf(i);
        }
        cboMaMau = new JComboBox(sColor);
        cboMaMau.setBounds(100, 200, 220, 20);
        cboMaMau.setSelectedIndex(0);
        cboMaMau.setRenderer(new MyCellRenderer(colors));
        cboMaMau.setEnabled(true);
        add(cboMaMau);
        
        //Ghi gia tri default
        if(this.option == ADD_PRODUCT){
            txtDate.setEnabled(false);   
            txtMaProduct.setText("SP");             
            LocalDate currentDate = LocalDate.now();
            txtDate.setText(String.valueOf(currentDate.getDayOfMonth())+
                    "/"+String.valueOf(currentDate.getMonthValue())+"/"
                    +String.valueOf(currentDate.getYear()));         
            txtHSD.setText(String.valueOf(currentDate.getDayOfMonth())+
                    "/"+String.valueOf(currentDate.getMonthValue())+"/"
                    +String.valueOf(currentDate.getYear()));

        }else if(this.option == EDIT_PRODUCT){
            txtMaProduct.setEnabled(false);
            LoadProduct();
        }
        //Create button Them
        JButton btnCancel = new JButton("Hủy");
        btnCancel.setBounds(260, 225, 60, 25);
        add(btnCancel);
        btnCancel.setActionCommand("Hủy");
        btnCancel.addActionListener(new closeListenner());
            
        btnAdd = new JButton(title);
        btnAdd.setBounds(200, 225, 60, 25);
        btnAdd.addActionListener(new menuListener());
        add(btnAdd);      
    }
    // them truoc hoac them sau
    private int TypeAdd(){
        int numberDrawer = area.getDrawernumber();
        int numbershelf = area.getShelfquantity();
        int totalDrawer = numberDrawer * numbershelf;
        if(col == totalDrawer - 1)
        {
             if(DetailSurface.m_Drawers[row][col - 1].productCode == -1)
             {
                 return ADD_PRODUCTBEFORE;
             }
        }
        if(col == 0)
        {
            if(DetailSurface.m_Drawers[row][col + 1].productCode == -1)
            {
                return ADD_PRODUCTAFTER;
            }
        }
        if(col > 0 && col < totalDrawer - 1)
        {
            if(DetailSurface.m_Drawers[row][col + 1].productCode == -1)
            {
                return ADD_PRODUCTAFTER;
            }
            if(DetailSurface.m_Drawers[row][col - 1].productCode == -1)
            {
                return ADD_PRODUCTBEFORE;
            } 
        }
        return NOT_ADD;
    }
    private boolean isNumber(String text){
        try {
            double number = Double.parseDouble(text);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    private boolean isDate(String text){
        try {
            String []info = text.split("/");
            int d = Integer.parseInt(info[0]);
            int m = Integer.parseInt(info[1]);
            int y = Integer.parseInt(info[2]);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    private boolean isAllowed(){
        if(txtMaProduct.getText().trim().isEmpty())
        {
            JOptionPane.showMessageDialog(null, "Mã sản phẩm bị rỗng!", "Cảnh báo", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        if(txtNameProduct.getText().trim().isEmpty())
        {
            JOptionPane.showMessageDialog(null, "Tên sản phẩm bị rỗng!", "Cảnh báo", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        if(txtPrice.getText().trim().isEmpty())
        {
            JOptionPane.showMessageDialog(null, "Giá sản phẩm bị rỗng!", "Cảnh báo", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        else
        {
            if(!isNumber(txtPrice.getText().trim()))
            {
                JOptionPane.showMessageDialog(null, "Giá sản phẩm không hợp lệ!", "Cảnh báo", JOptionPane.WARNING_MESSAGE);
                return false;
            }
        }
        if(txtDate.getText().trim().isEmpty())
        {
            JOptionPane.showMessageDialog(null, "Ngày nhập kho bị rỗng!", "Cảnh báo", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        else
        {
            if(!isDate(txtDate.getText().trim()))
            {
                JOptionPane.showMessageDialog(null, "Ngày nhập kho không hợp lệ!", "Cảnh báo", JOptionPane.WARNING_MESSAGE);
                return false;
            }
        }
        if(txtHSD.getText().trim().isEmpty())
        {
            JOptionPane.showMessageDialog(null, "Hạn sử dụng bị rỗng!", "Cảnh báo", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        else
        {
            if(!isDate(txtHSD.getText().trim()))
            {
                JOptionPane.showMessageDialog(null, "Hạn sử dụng không hợp lệ!", "Cảnh báo", JOptionPane.WARNING_MESSAGE);
                return false;
            }
        }
        int sl = Integer.parseInt(cboSLOKH.getSelectedItem().toString());
        if(sl > 1)
        {
            typeadd = TypeAdd();
            if(typeadd == NOT_ADD)
            {
                JOptionPane.showMessageDialog(null, "Số ngăn không hợp lệ!", "Cảnh báo", JOptionPane.WARNING_MESSAGE);
                return false;
            }
        }
        return true;
    }
    private void PrintProduct(Product product)
    {
        String info = product.getPosRow() + " " + product.getPosCol() + " " 
                    + product.getIDProduct() + " (" + product.getNameProduct() + ") "
                    + product.getPrice() + " "  
                    + product.getDate() + " " + product.getEndDay() + " "
                    + product.getContainer() + " " + product.getColorCode();
        System.out.println(info);
    }
    
    private void AddProduct()
    {
        int posRow = row + 1;
        int posCol = col + 1;
        Product p = new Product();
        if(typeadd == ADD_PRODUCTBEFORE)
        {
            posCol = col;
        }
        p.setPosRow(posRow);
        p.setPosCol(posCol);
        p.setIDProduct(txtMaProduct.getText().trim());
        p.setNameProduct(txtNameProduct.getText().trim());
        p.setPrice(Double.parseDouble(txtPrice.getText().trim()));
        p.setDate(txtDate.getText().trim());
        p.setEndDay(txtHSD.getText().trim());
        p.setContainer(Integer.parseInt(cboSLOKH.getSelectedItem().toString()));
        p.setColorCode(Integer.parseInt(cboMaMau.getSelectedItem().toString()));

        ArrayList<Product> lstProduct = area.getListProducts();
        lstProduct.add(p);
    }
    private void LoadProduct()
    {
        Product p = this.area.getListProducts().get(posProduct);
        txtMaProduct.setText(p.getIDProduct());
        txtNameProduct.setText(p.getNameProduct());
        txtPrice.setText(p.getPrice() + "");
        txtDate.setText(p.getDate());
        txtHSD.setText(p.getEndDay());
        cboSLOKH.setSelectedItem(p.getContainer() + "");
        cboMaMau.setSelectedItem(p.getColorCode() + "");
    }
    private void EditProduct()
    {
        Product p = this.area.getListProducts().get(posProduct);
        p.setIDProduct(txtMaProduct.getText().trim());
        p.setNameProduct(txtNameProduct.getText().trim());
        p.setPrice(Double.parseDouble(txtPrice.getText().trim()));
        p.setDate(txtDate.getText().trim());
        p.setEndDay(txtHSD.getText().trim());
        p.setContainer(Integer.parseInt(cboSLOKH.getSelectedItem().toString()));
        p.setColorCode(Integer.parseInt(cboMaMau.getSelectedItem().toString()));
        PrintProduct(p);
    }
    public class menuListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if(option == ADD_PRODUCT){
                if(isAllowed())
                {
                    AddProduct();
                    client.sendObject("update");
                    UpdateInfo info = new UpdateInfo(Surface.posDrawer, area);
                    client.sendObject(info);
                    dispose();
                } 
            }else if(option == EDIT_PRODUCT){
                if(isAllowed())
                {
                    EditProduct();
                    client.sendObject("update");
                    UpdateInfo info = new UpdateInfo(Surface.posDrawer, area);
                    client.sendObject(info);
                    dispose();
                }
            }
        }
    };
    
    public class closeListenner implements ActionListener{
        public void actionPerformed(ActionEvent e){
                dispose();
        }
    }
}