/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Cao
 */
public class InfoTransaction extends JDialog{
    public static ArrayList<String> lstContent = null;
    JTextArea jtextArea = null;
    public InfoTransaction()
    {
        initUI();
    }
    private void initUI()
    {
        setModal(true);
        setResizable(false);
        setTitle("Thông tin");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setSize(700, 700);
        setLayout(new BorderLayout());
        
        JScrollPane jscroll = new JScrollPane();
        jtextArea = new JTextArea();
        jscroll.setViewportView(jtextArea);
        add(jscroll, BorderLayout.CENTER);
        setResizable(false);
        setLocationRelativeTo(null);
        LoadInfo();
    }
    private void LoadInfo()
    {
        String s = "";
        for(int i = 0; i < lstContent.size(); i++)
        {
            s += lstContent.get(i) + "\n";
        }
        jtextArea.setText(s);
    }
}
