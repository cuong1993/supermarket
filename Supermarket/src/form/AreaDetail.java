/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import surface.DetailSurface;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import models.SupermarketArea;
import network.Client;
/**
 *
 * @author Cao
 */
public class AreaDetail extends JDialog {
    private SupermarketArea area = null;
    private Client client = null;
    
    public AreaDetail(SupermarketArea area, Client client){
        this.area = area;
        this.client = client;
        initUI();
    }

    private void initUI(){        
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                if(DetailSurface.idProduct.size() > 0)
                {
                    JOptionPane.showMessageDialog(null, "Vui lòng xuất hóa đơn trươc khi thoát!", "Thông báo", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                else
                {
                    dispose();
                }
            }
        });
        
        setModal(true);
        setResizable(false);
        setTitle("Area Detail");
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        
        DetailSurface detail = new DetailSurface(area, this.client);
        add(detail,BorderLayout.CENTER);
        setSize(1000, 600);
        setLocationRelativeTo(null);
    }  
}
