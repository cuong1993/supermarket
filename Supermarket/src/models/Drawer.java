/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import models.Product;

/**
 *
 * @author Cao
 */
public class Drawer extends Rectangle2D.Float {
    public int productCode = -1;
    public int numberDrawer = 0;// moi san pham chiem may o trong ke hang
    public int vtNumberDrawer = -1;// cai nay la vi tri click chuot o 1 or o 2
    public int vtIJ_Drawer = -1;//cai nay danh dau nhung o nao chiem 2 san pham thi danh dau x
    public int colorCode = -1;
    public boolean isHidden = false;
    public int x1 = -1;
    public int y1 = -1;
    public Drawer(float x, float y, float width, float height) {
        this.x1 = (int)x;
        this.y1 = (int)y;
        setRect(x, y, width, height);
    }
    public boolean isHit(float x, float y) {
            return getBounds2D().contains(x, y);
    }
 
    public void addX(float x) {     
        this.x += x;
    }
 
    public void addY(float y) {     
        this.y += y;
    }
    
    public void CopyAtoB(Drawer a){
        this.productCode = a.productCode;
        this.numberDrawer = a.numberDrawer;
        this.vtNumberDrawer = a.vtNumberDrawer;
        this.vtIJ_Drawer = a.vtIJ_Drawer;
        this.colorCode = a.colorCode;
        this.isHidden = a.isHidden;
    }
    public void setDefaultDrawer(){
        this.productCode  = -1;
        this.numberDrawer = 0;
        this.vtNumberDrawer = -1;
        this.vtIJ_Drawer = -1;
        this.colorCode = -1;
        this.isHidden = false;
    }
}

